interface VerboseConsole extends Console {
    
}
declare global {
    interface Console {
        verbose(fmt, ...args: string[]): void;
    }
}