import { Sign, randomUUID } from 'crypto';

import fs from 'fs';
import proc from 'process';
import express from 'express';
import bodyParser from 'body-parser';
import childProcess from 'child_process';
import { SignedHeaderStruct } from './readRWFirmwareHeaders.js';
import path, { dirname } from 'path';
// import { } from './globals.mjs';
import QueryString from 'qs';
import stream from 'stream';
global.__dirname = proc.cwd();
//Responses may cut off the buffer is too big. The below function will resolve this bug.
/**
 * 
 * @param {Buffer} buf 
 * @param {express.Response} res 
 * @param {number | undefined} chunkSize
 */
function writeLargeBuffer(buf, res, chunkSize) {
    // res.writeHead(200);
    let written = 0;
    let size = chunkSize ?? 1024;
    let finished = false;
    function readFromBuf(off, size) {
        return buf.subarray(off, off+size);
    }
    function writeToStream() {
        if (written >= buf.length) {
            size = buf.length % 1024;

        }
        if (written === buf.length) {
            // We're done here
            return 0;
        }
        var shouldWaitForDrain = !res.write(readFromBuf(written, size));
        written += size;
        if (shouldWaitForDrain) {
            // Wait till drain to do the next write.
            res.once('drain', writeToStream);
        }
        return writeToStream();
    }
    
    return writeToStream();
}
function mkTemp(prefix, uid) {
    return "/tmp/" + prefix + uid ?? randomUUID().replace('-','_');
}
class Cr50CodeSigner {
    #path;
    constructor(path) {
        this.#path = path;
    }

    /**
     * 
     * @param  {...string} argv 
     */
    #run(...argv) {
        return new Promise((r, rej) => {

            console.log(this.#path);
            var argument = argv;
            var binaryPath = this.#path;
            var buf = childProcess.spawn(this.#path, argument, {
                stdio: "pipe"
            });
            // There is no message
            // buf.on('message', (msg) => {
            //     if (typeof msg === 'string') {
            //         console.verbose(msg);
                    
            //     }
            // });
            buf.stdout.on('data', (d)=>{
                console.log(d.toString('utf-8'));
            });
            buf.on("error", (e) => {
                console.error(e);
                rej(e);
            });
            buf.on('disconnect', (e) => {
                console.error("Unexpected disconnect from child process.");
            })
            buf.on('exit', (code) => {
                // The exit code is meaningless, but we can continue from here
                r(code);

            });
        });
    }
    /**
     * @param {string} tempImgPath
     * @param {string} tempImgOutput
     * @param {string} [board_id]
     * @param {string} [dev_id]
     * @param {"RO"|"RW"} [fwType]
     * @param {string} [uid]
     * @param {{
     *      epoch: number,
     *      minor: number,
     *      major: number
     *  }} [version]
     * @return {Promise<number>} Exit code, when the process is ran asynchronously
     */
    signImage(tempImgPath, tempImgOutput, board_id, dev_id, fwType, uid, version) {
        // Dev keyid: -764428053

        
        fwType = fwType ?? "RO";
        let signerPath = path.resolve(__dirname, proc.argv[2]);
        let manifestPath = path.resolve(signerPath, 'ec_RW-manifest-dev.json');
        let jso = fs.readFileSync(manifestPath);
        let o = JSON.parse(jso);
        o.minor = version.minor;
        o.major = version.major;
        o.epoch = version.epoch;
        //ROM Test keyid or RW Test keyid
        let keyid = fwType === "RW" ? -764428053 : 269140155 ;
        o.keyid = keyid;

        let jsonString = JSON.stringify(o, null, '\n');
        var manifestFile = mkTemp('temp_image_manifest_', uid+".json");
        fs.writeFileSync(manifestFile, jsonString);
console.log(manifestFile);
        let argv = ['--input', tempImgPath, '--output', tempImgOutput, '--key', path.resolve(__dirname, proc.argv[2], fwType === "RW" ? 'loader-testkey-A.pem' : "rom-testkey-A.pem"), "--json", manifestFile, "--xml", path.resolve(signerPath, 'fuses.xml'), '--format', 'bin'];
        if (board_id) {
            argv.push('--board_id', board_id);
        }
        if (dev_id) {
            argv.push('--dev_id', dev_id);
        }
        console.log("Using arguments: "+argv.join(' '));
        
        return this.#run(...argv);
    }

    static createCodeSigner() {
        return new Cr50CodeSigner(path.resolve(proc.cwd(), proc.argv[2], 'cr50-codesigner'))
    }
}
(async () => {
    function usage() {
        console.log("<path of closed source signer>");
        return -1; // Invalid parameters
    }
    if (proc.argv.length < 2) {
        usage();
        return;
    }
    console.verbose = (str, ...args) => {
        console.debug(str, ...args);
    }
    
    /**
     * 
     * @param {express.Request} req .
     * @param {express.Response} res
     */
    async function signImage(req, res) {
        /**
         * @type {Buffer}
         */
        var imageBuffer = req.body;
        var queryParam = req.query;
        
        console.log("Signing image from " + req.socket.remoteAddress);
        // Lets first go through the closed source code signer

        var ab = imageBuffer.buffer;
        // var s = new ruct(1024);
        var uidFile = randomUUID().replace('-', '_');
        var temp_image = '/tmp/temp_image_' + uidFile;
        // var temp_image_ro = '/tmp/temp_image_ro_'+randomUUID().replace('-', '_');
        fs.writeFileSync(temp_image, imageBuffer);
        var out_img = '/tmp/temp_image_out_' + uidFile;
        
        var board_id;
        var dev_id, fwType;
        if (queryParam['board_id'] && typeof queryParam['board_id'] === 'string') {
            board_id = queryParam['board_id'];
        }else {
            res.writeHead(500);
            res.end("Invalid query param: board_id");
            return;
        }
        if (queryParam['fwtype'] && typeof queryParam['fwtype'] === 'string') {
            fwType = queryParam['fwtype'];
        } 
        if (queryParam['dev_id'] && typeof queryParam['dev_id'] === 'string') {
            dev_id = queryParam['dev_id'];
            
        } 
        // var codesignerPath = path.resolve(__dirname, proc.argv[2], 'cr50-codesigner');
        const codeSigner = Cr50CodeSigner.createCodeSigner();
        function parseVersion(verstr) {
            if (!(typeof verstr === 'string' || verstr instanceof String)) {
                return;
            }
            
            var v = verstr
            var idx = 0;
            var versionArr = [];
            var begin = 0, end = 0;
            for (var i = 0; i < v.length; i++) {
                
                if (v[i] === ".") {
                    versionArr.push(parseInt(v.substring(begin, i)));
        
                    begin = i + 1;
                    continue;
                }
                
        
            }
            versionArr.push(parseInt(v.substring(begin)));
            // console.log(versionArr);
            return {
                epoch: versionArr[0],
                major: versionArr[1],
                minor: versionArr[2]
            }
        }
        var exitCode;
        try {
            exitCode = await codeSigner.signImage(temp_image, out_img, board_id, dev_id, fwType, uidFile, parseVersion(queryParam['version']));
            // console.log("There was an error: " + exitCode);

            var buffer = fs.readFileSync(out_img);
            if (buffer) {
                console.log("SUCESS!!! on executing the code signer");
                console.log(buffer.length);
                //Time to use signed header struct
                // var shs = new SignedHeaderStruct()
                fs.rmSync(out_img);
                fs.rmSync(temp_image);
                res.setHeader('Content-Type', 'application/octet-stream');
                res.setHeader('Content-Length', buffer.byteLength);
                res.writeHead(200);
                res.write(buffer, 'binary');
                return;
            } else {
            }
        } catch (e) {
            res.writeHead(404, "Error");
            res.end("Error: " + e);
        }


    }
    if (proc.argv.length < 3) {
        usage();
    }
    var isValidDir = false;
    var rds = fs.readdirSync(proc.argv[2]);
    for (var v of rds) {
        if (v === "cr50-codesigner") {
            isValidDir = true;
        }
    }
    if (!isValidDir) {
        console.log("No valid dir was found.");
        return -1;
    }
    console.log("hi");
    const app = express();
    // app.get("/", (req, res) => {
    //     res.writeHead(200);
    //     res.end("Only post requests are accepted");
    // })
    const options = {
        type: 'application/octet-stream',
    };
    app.use(bodyParser.raw({ "type": "application/octet-stream" , "limit": "10MB"}));
    
    app.post("/sign", (req, res) => {
        // if (req.url != "/sign") {
        //     res.writeHead(404, "Not Found");
        //     res.end("404 Not Found!");
        // }
        console.log(req.headers);
        if (!req.body) {
            res.writeHead(200);
            res.end("Invalid body data.");

        }
        console.log(req.body.length);
        signImage(req, res);

    });
    app.get("/sign", (req, res)=>{
        var buffer = Buffer.from("<h1 style=\"font-family: Helvetica, Arial;\">You should only access this url from CRSwifty.</h1>", "utf-8");
        res.setHeader('Content-Type', "text/html");
        res.setHeader('Content-Length', buffer.byteLength);

        res.writeHead(404, "Not found");
        res.end(buffer);
    })
    app.listen(8080, "0.0.0.0", () => {
        // console.log(e);
        console.log("Serving on 8080");
    });
    app.use('/', express.static('docs'));
    app.use("/client", express.static('websignerclient', {
        index: ['index.html']
    }));
    app.use("/client", (req,res)=>{
        res.writeHead(404);
        res.end("404: Not found");

    })
    app.use("/download", express.static('websignerclient/download'));
    app.get("/build/crswifty", (req, res)=>{
        var buf = fs.readFileSync('build/crswifty');
        res.setHeader('Content-Type', 'application/octet-stream');
        res.setHeader("Content-Length", buf.byteLength);
        res.writeHead(200);
        res.end(buf, 'binary');
    });
    // while (true);
    // Now we can ensure that the closed-source signer is somewhere
    // Check the location of signer.zip extracted


})().then((error) => {
    // proc.exit(id);
});