var proc = require('process');
var fs = require('fs');
const { debug } = require('console');
(async () => {
    if (proc.argv.length < 4) {
        console.log("Usage: <Firmware File> <Part name>");
        return;
    }
    var firmwareFileName = proc.argv[2];
    var buf = fs.readFileSync(firmwareFileName).buffer;
    var ua = new Uint8Array(buf);
    var signedHeaderStruct = ua.subarray(0, 1024);
    function max(a, b) {
        return a > b ? a : b;
    }
    class Serializable {
        static STRUCT_SIZE = 1024;
        buf;
        constructor(size) {
            var len = max(size, Serializable.STRUCT_SIZE);
            this.buf = new DataView(new ArrayBuffer(len));
        }
        // read(type)
        serialize() {
            return this.buf;
        }
    }
    class BufferHolder {

        nativeBuf = null;
        writePosition = 0;
        readPosition = 0;
        size = 1;
        /**
         * @type {DataView}
         */
        dv = null;
        littleEndian = true;
        constructor(size, position, littleEndian = true, originalData) {
            this.nativeBuf = originalData ? originalData.buffer : new ArrayBuffer(size);
            this.writePosition = 0;
            this.readPosition = 0;
            this.size = size;
            this.dv = new DataView(this.nativeBuf);
            this.littleEndian = littleEndian;
        }
        rewindWriter() {
            this.writePosition = 0;

        }
        rewindReader() {
            this.readPosition = 0;
        }
        writeU8(v) {
            this.dv.setUint8(this.writePosition, v, this.littleEndian);
            this.writePosition += 1;
        }
        writeU16(v) {
            this.dv.setUint16(this.writePosition, v, this.littleEndian);
            this.writePosition += 2;
        }
        writeU32(v) {
            this.dv.setUint32(this.writePosition, v, this.littleEndian);
            this.writePosition += 4; // Sizeof uint32 in bytes is 4. Each byte is 8 bits.
        }

        writeInt32(v) {
            this.dv.setInt32(this.writePosition, v, this.littleEndian);
            this.writePosition += 4;
        }
        writeInt16(v) {
            this.dv.setInt16(this.writePosition, v, this.littleEndian);
            this.writePosition += 2;
        }
        writeInt8(v) {
            this.dv.setInt8(this.writePosition, v);
            this.writePosition += 1;
        }
        writeUint64(v) {
            this.dv.setBigUint64(this.writePosition, v);
            this.writePosition += BigUint64Array.BYTES_PER_ELEMENT;
        }
        writeU32Arr(v) {
            var buf = new Uint32Array(this.nativeBuf, this.writePosition);
            buf.set(v);
            this.writePosition += 4 * v.length;
        }

        writeU16Arr(v) {
            var buf = new Uint16Array(this.nativeBuf, this.writePosition);
            buf.set(v);
            this.writePosition += Uint16Array.BYTES_PER_ELEMENT * v.length;
        }
        writeU8Arr(v) {
            var buf = new Uint8Array(this.nativeBuf, this.writePosition);
            buf.set(v);
            this.writePosition += Uint8Array.BYTES_PER_ELEMENT * v.length;
        }
        writeUint64Arr(v) {
            var buf = new BigUint64Array(this.nativeBuf, this.writePosition);
            buf.set(v);
            this.writePosition = BigUint64Array.BYTES_PER_ELEMENT * 4;
        }
        writeInt32(v) {
            var buf = new Int32Array(this.nativeBuf, this.writePosition);
            buf.set(v);
            this.writePosition += 4 * v.length;
        }
        writeInt16(v) {
            var buf = new Int16Array(this.nativeBuf, this.writePosition);
            buf.set(v);
            this.writePosition += 2 * v.length;
        }
        writeInt8(v) {
            var buf = new Int8Array(this.nativeBuf, this.writePosition);
            buf.set(v);
            this.writePosition += 1 * v.length;
        }
        writeInt64(v) {
            var buf = new BigInt64Array(this.nativeBuf, this.writePosition);
            buf.set(v);
            this.writePosition += 8 * v.length;
        }
        readU8() {
            var rp = this.readPosition;
            this.readPosition++;
            return this.dv.getUint8(rp);
        }
        readU16() {
            var rp = this.readPosition;
            this.readPosition += 2;
            return this.dv.getUint16(rp);
        }
        readU32() {
            var rp = this.readPosition;
            this.readPosition += 4;
            return this.dv.getUint32(rp, this.littleEndian);
        }
        readInt32() {
            var rp = this.readPosition;
            this.readPosition += 4;
            return this.dv.getInt32(rp, this.littleEndian);
        }
        readInt16() {
            var rp = this.readPosition;
            this.readPosition += 2;
            return this.dv.getInt16(rp, this.littleEndian);
        }
        readInt8() {
            var rp = this.readPosition;
            this.readPosition++;
            return this.dv.getInt8(rp);
        }
        readU64() {
            var rp = this.readPosition;
            this.readPosition += 8;
            return this.dv.getBigUint64(rp, this.littleEndian); // Little Endian is for struct serialization
        }
        readU64Arr(len) {
            var buff = new BigUint64Array(this.nativeBuf, this.readPosition, len);
            var cpy = new BigUint64Array(len);
            cpy.set(buff);
            this.readPosition += BigUint64Array.BYTES_PER_ELEMENT;
            return cpy;
        }
        readU32Arr(len) {
            var buff = new Uint32Array(this.nativeBuf, this.readPosition, len);
            var copy = new Uint32Array(len);
            copy.set(buff, 0);
            this.readPosition += 4 * len;
            return copy;
        }
        skip(len) {
            this.readPosition += len;
        }
        readU16Arr(len) {
            var buff = new Uint16Array(this.nativeBuf, this.readPosition, len);
            var copy = new Uint16Array(len);
            copy.set(buff);
            this.readPosition += 2 * len;
            return copy;
        }
        readU8Arr(len) {
            var buff = new Uint8Array(this.nativeBuf, this.readPosition, len);
            var copy = new Uint8Array(len);
            this.readPosition += 1 * len;
            copy.set(buff);
            return copy;
        }
        readInt8Arr(len) {
            var buff = new Int8Array(this.nativeBuf, this.readPosition, len);
            var copy = new Int8Array(len);
            this.readPosition += 1 * len;
            copy.set(buff);
            return copy;
        }
        readInt16Arr(len) {
            var buff = new Int16Array(this.nativeBuf, this.readPosition, len);
            var copy = new Int16Array(len);
            this.readPosition += 2 * len;
            copy.set(buff);
            return copy;
        }
        readInt32Arr(len) {
            var buff = new Int32Array(this.nativeBuf, this.readPosition, len);
            var copy = new Int32Array(len);
            this.readPosition += 4 * len;
            copy.set(buff);
            return copy;
        }
        readInt64Arr(len) {
            var buff = new BigInt64Array(this.nativeBuf, this.readPosition, len);
            var copy = new BigInt64Array(len);
            copy.set(buff);
            this.readPosition += 8 * len;
            return copy;
        }

    }
    function makeUtilityBuffer(size) {
        return new Uint8Array(size);
    }

    class FieldIterator {

        idx = 0;
        treeIdx = 0;
        objTree = [];
        cachedKeys = [];
        prevTreeIndex = 0;
        finished = false;
        constructor(obj) {
            console.log("[INFO] Getting field iter ready.")
            // this.idx = 0;
            // this.treeIdx = 0;
            this.objTree.push(obj);
            this.cachedKeys = Object.keys(obj);
            this.treeIdx = 0;
            // Let next() figure it out

            // This line make sure that the first is selected if valid

            this.idx = -1;

            this.next();
        }
        checkForOutOfBoundsInFields(i) {
            if ((i) >= this.cachedKeys.length) {
                // There are no more items to read. SUbstract treeidx by 1 to backout to root.
                //but if... we are already in root, we need to terminate iterator by setting all objects to null.
                if (this.treeIdx === 0) {
                    this.finished = true;

                    return;
                }
                this.treeIdx -= 1;
                this.idx = this.prevTreeIndex;
                this.cachedKeys = Object.keys(this.objTree[this.treeIdx]);
                this.objTree.pop();
                this.next();
                return;
            }
        }
        set value(v) {

            this.objTree[this.treeIdx][this.cachedKeys[this.idx]] = v;
        }
        get value() {
            return this.objTree[this.treeIdx][this.cachedKeys[this.idx]];
        }
        static isValidField(o, key) {
            //Get current field
            var field = o[key];
            if (!field) return true; // Uninitalied values are valid fields
            if (field instanceof Function) {
                // skip by returning false. we cannot serialize functions.
                return false;
            }
            if (!(["bigint", "number", "object", "string", "boolean"].includes(typeof field))) {
                // skip anything that isn't any of the types above, this includes primitives
                return false;
            }
            return true;
        }
        getFieldName() {
            return this.cachedKeys[this.idx];
        }
        dummyIdx;
        next() {
            // debugger;
            this.dummyIdx = 0;
            this.checkForOutOfBoundsInFields(this.idx + 1);
            // var local = 0;
            for (var i = this.idx + 1; i < this.cachedKeys.length; i++) {

                // this.checkForOutOfBoundsInFields(i);
                var field = this.cachedKeys[i];
                if (FieldIterator.isValidField(this.objTree[this.treeIdx], field)) {
                    if (this.objTree[this.treeIdx][field] instanceof Object) {
                        //Treat this like a union and change treeIdx.
                        this.prevTreeIndex = i;
                        this.treeIdx = this.objTree.push(this.objTree[this.treeIdx][field]) - 1;
                        this.idx = -1;
                        this.cachedKeys = Object.keys(this.objTree[this.treeIdx]);
                        return this.next();
                        // continue;
                    }
                    this.idx = i;
                    return;
                };
                this.dummyIdx = i;
            }
            // No results found, try again outside
            this.checkForOutOfBoundsInFields(this.dummyIdx + 1);
            // this.next();
        }
    }
    class SignedHeaderStruct extends Serializable {

        magic;
        signature;
        img_chk_; // Uint32
        tag; //uint32[7]
        keyid;// uint32
        key;// Uint32[96]
        image_size; // Uint32
        ro_base; // uint32
        ro_max; // uint32
        rx_base; // uint32
        rx_max; // uint32
        fusemap; // uint32 []
        infomap; // uint32
        epoch_; // uint32
        major_; // uint32
        minor_; // uint32
        timestamp_; // uint32
        p4cl_; // uint32
        applysec_; // uint32
        config1_; // uint32
        err_response_; // uint32
        expect_response_; // uint32
        u = {
            ext_sig: {
                keyid: null,  //uint32[8]
                r: null,  //uint32
                s: null  //uint32
            },
            fsh: {
                FSH_SMW_SETTING_OPTION3: null, // Uint32
                FSH_SMW_SETTING_OPTION2: null, // Uint32
                FSH_SMW_SETTING_OPTIONA: null, // Uint32
                FSH_SMW_SETTING_OPTIONB: null, // Uint32
                FSH_SMW_SMP_WHV_OPTION1: null, // Uint32
                FSH_SMW_SMP_WHV_OPTION0: null, // Uint32
                FSH_SMW_SME_WHV_OPTION1: null, // Uint32
                FSH_SMW_SME_WHV_OPTION0: null  // Uint32
            }
        }; // Union
        _pad; // nonsense
        swap_mark;// number (uint32)

        serialize() {

            return this.buf;
        }


        /**
         * 
         * @param {SignedHeaderStruct} shs 
         * @param {BufferHolder} bh 
         * @param {number} off 
         * @param {number} count 
         * @returns {SignedHeaderStruct}
         */
        static writeU32Fields(shs, bh, off, count) {
            if (!(shs instanceof SignedHeaderStruct)) return;
            var ok = Object.keys(shs);
            for (var i = off; i < off + count; i++) {
                shs[ok[i]] = bh.readU32();
            }
            return shs;

        }
        static deserializeSignedHeader(buffer) {
            const holder = new BufferHolder(buffer.length, null, true, buffer);

            // const magic = holder.readU32();


            // const signature = holder.readU32Arr(96);
            // const img_chk_ = holder.readU32();
            // const tag = holder.readU32Arr(7);
            // const keyid = holder.readU32();
            // const key = holder.readU32Arr(96);
            // const image_size = holder.readU32();
            // const ro_base = holder.readU32();
            // const ro_max = holder.readU32();
            // const rx_base = holder.readU32();
            // const rx_max = holder.readU32Arr(4);
            // const fusemap = holder.readU32Arr(16 * 8);
            // const infomap = holder.readU32Arr(8 * 8);
            // const epoch_ = holder.readU32();
            // const major_ = holder.readU32();
            // const minor_ = holder.readU32();
            // const timestamp_ = holder.readUint64();
            // const p4cl_ = holder.readU32();
            // const applysec_ = holder.readU32();
            // const config1_ = holder.readU32();
            // const err_response_ = holder.readU32();
            // const expect_response_ = holder.readU32();
            // const u = holder.readU32();
            // const ext_sig = {
            //     keyid: holder.readU32(),
            //     r: holder.readU32Arr(8),
            //     s: holder.readU32Arr(8)
            // };
            // const _pad = holder.readU32Arr(5);
            // const swap_mark = {
            //     size: holder.readUint16(),
            //     offset: holder.readUint16()
            // };
            // const rw_product_family_ = holder.readU32();
            // const board_id_type = holder.readU32();
            // const board_id_type_mask = holder.readU32();
            // const board_id_flags = holder.readU32();
            // const dev_id0_ = holder.readU32();
            // const dev_id1_ = holder.readU32();
            // const fuses_chk_ = holder.readU32();
            // const info_chk_ = holder.readU32();

            // return {
            //     magic,
            //     signature,
            //     img_chk_,
            //     tag,
            //     keyid,
            //     key,
            //     image_size,
            //     ro_base,
            //     ro_max,
            //     rx_base,
            //     rx_max,
            //     fusemap,
            //     infomap,
            //     epoch_,
            //     major_,
            //     minor_,
            //     timestamp_,
            //     p4cl_,
            //     applysec_,
            //     config1_,
            //     err_response_,
            //     expect_response_,
            //     u,
            //     ext_sig,
            //     _pad,
            //     swap_mark,
            //     rw_product_family_,
            //     board_id_type,
            //     board_id_type_mask,
            //     board_id_flags,
            //     dev_id0_,
            //     dev_id1_,
            //     fuses_chk_,
            //     info_chk_
            // };
            const magic = holder.readU32();
            const signature = holder.readU32Arr(96);
            const img_chk_ = holder.readU32();
            const tag = holder.readU32Arr(7);
            const keyid = holder.readU32();
            const key = holder.readU32Arr(96);
            const image_size = holder.readU32();
            const ro_base = holder.readU32();
            const ro_max = holder.readU32();
            const rx_base = holder.readU32();
            const rx_max = holder.readU32();
            const fusemap = holder.readU32Arr(4);
            const infomap = holder.readU32Arr(4);
            const epoch_ = holder.readU32();
            const major_ = holder.readU32();
            const minor_ = holder.readU32();
            const timestamp_ = holder.readU64();
            const p4cl_ = holder.readU32();
            const applysec_ = holder.readU32()
            const config1_ = holder.readU32();
            const err_response_ = holder.readU32();
            const expect_response_ = holder.readU32();
            var fsh;
            var ext_sig;
            if (config1_ & 65565 > 0) {
                 fsh = {
                    FSH_SMW_SETTING_OPTION3: holder.readU32(),
                    FSH_SMW_SETTING_OPTION2: holder.readU32(),
                    FSH_SMW_SETTING_OPTIONA: holder.readU32(),
                    FSH_SMW_SETTING_OPTIONB: holder.readU32(),
                    FSH_SMW_SMP_WHV_OPTION1: holder.readU32(),
                    FSH_SMW_SMP_WHV_OPTION0: holder.readU32(),
                    FSH_SMW_SME_WHV_OPTION1: holder.readU32(),
                    FSH_SMW_SME_WHV_OPTION0: holder.readU32()
                }
           
            } else {
                console.log("Detected gnubby chip. This may not be supported")

                 ext_sig = {
                    keyid: holder.readU32(),
                    r: holder.readU32Arr(8),
                    s: holder.readU32Arr(8)
                }
            }
            const u = {
                ext_sig,
                fsh
            };
            holder.skip(68-32);
            const _pad = holder.readU32Arr(5);
            const swap_mark = holder.readU32();

            const rw_product_family_ = holder.readU32();
            const board_id_type = holder.readU32();
            const board_id_type_mask = holder.readU32();
            const board_id_flags = holder.readU32();
            const dev_id0_ = holder.readU32();
            const dev_id1_ = holder.readU32();
            const fuses_chk_ = holder.readU32();
            const info_chk_ = holder.readU32();

            return {
                magic,
                signature,
                img_chk_,
                tag,
                keyid,
                key,
                image_size,
                ro_base,
                ro_max,
                rx_base,
                rx_max,
                fusemap,
                infomap,
                epoch_,
                major_,
                minor_,
                timestamp_,
                p4cl_,
                applysec_,
                config1_,
                err_response_,
                expect_response_,
                u,
                _pad,
                swap_mark,
                rw_product_family_,
                board_id_type,
                board_id_type_mask,
                board_id_flags,
                dev_id0_,
                dev_id1_,
                fuses_chk_,
                info_chk_

            };
        }
        _initFields(o) {
            var keys = Object.keys(o);
            for (let i = 0; i < keys.length; i++) {
                this[keys[i]] = o[keys[i]];
            }
        }
        static deserialze(ua) {
            if (!(ua instanceof Uint8Array)) {
                console.log("Pass in valid data");
                return null;
            }
            if (ua.length != 1024) {
                // console.log("Invalid size, exiting to prevent invalid data.");
                return null;
            }
            var utilBuf = makeUtilityBuffer(1024);
            var bh = new BufferHolder(ua.length, 0, true, ua);
            var shs = new SignedHeaderStruct(max(ua.length, 1024));
            shs._initFields(this.deserializeSignedHeader(new Uint8Array(bh.nativeBuf)));
            // var fi = new FieldIterator(shs);


            return shs;
        }
        static serialize(header) {

            const holder = new BufferHolder(new ArrayBuffer(1024));

            holder.writeU32(header.magic);
            holder.writeU32Arr(header.signature);
            holder.writeU32(header.img_chk_);
            holder.writeU32Arr(header.tag);
            holder.writeU32(header.keyid);
            holder.writeU32Arr(header.key);
            holder.writeU32(header.image_size);
            holder.writeU32(header.ro_base);
            holder.writeU32(header.ro_max);
            holder.writeU32(header.rx_base);
            holder.writeU32Arr(header.rx_max);
            holder.writeU32Arr(header.fusemap);
            holder.writeU32Arr(header.infomap);
            holder.writeU32(header.epoch_);
            holder.writeU32(header.major_);
            holder.writeU32(header.minor_);
            holder.writeUint64(header.timestamp_);
            holder.writeU32(header.p4cl_);
            holder.writeU32(header.applysec_);
            holder.writeU32(header.config1_);
            holder.writeU32(header.err_response_);
            holder.writeU32(header.expect_response_);
            // holder.writeU32(header.u);
            holder.writeU32(header.u.ext_sig.keyid);
            holder.writeU32Arr(header.u.ext_sig.r);
            holder.writeU32Arr(header.u.ext_sig.s);

            holder.writeU32(header.u.fsh.FSH_SMW_SETTING_OPTION3);
            holder.writeU32(header.u.fsh.FSH_SMW_SETTING_OPTION2);
            holder.writeU32(header.u.fsh.FSH_SMW_SETTING_OPTIONA);
            holder.writeU32(header.u.fsh.FSH_SMW_SETTING_OPTIONB);
            holder.writeU32(header.u.fsh.FSH_SMW_SMP_WHV_OPTION1);
            holder.writeU32(header.u.fsh.FSH_SMW_SMP_WHV_OPTION0);
            holder.writeU32(header.u.fsh.FSH_SMW_SME_WHV_OPTION1);
            holder.writeU32(header.u.fsh.FSH_SMW_SME_WHV_OPTION0);
            
            holder.writeU32Arr(header._pad, 5);
            holder.writeU32(header.swap_mark);
            holder.writeU32(header.rw_product_family_);
            holder.writeU32(header.board_id_type);
            holder.writeU32(header.board_id_type_mask);
            holder.writeU32(header.board_id_flags);
            holder.writeU32(header.dev_id0_);
            holder.writeU32(header.dev_id1_);
            holder.writeU32(header.fuses_chk_);
            holder.writeU32(header.info_chk_);
            
            return holder.nativeBuf;

        }
    }
   var shs = SignedHeaderStruct.deserialze(signedHeaderStruct);
    console.log(shs);
})();


// struct SignedHeader {
// 	uint32_t magic;       /* -1 (thanks, boot_sys!) */
// 	uint32_t signature[96];
// 	uint32_t img_chk_;    /* top 32 bit of expected img_hash */
// 	/* --------------------- everything below is part of img_hash */
// 	uint32_t tag[7];      /* words 0-6 of RWR/FWR */
// 	uint32_t keyid;       /* word 7 of RWR */
// 	uint32_t key[96];     /* public key to verify signature with */
// 	uint32_t image_size;
// 	uint32_t ro_base;     /* readonly region */
// 	uint32_t ro_max;
// 	uint32_t rx_base;     /* executable region */
// 	uint32_t rx_max;
// 	uint32_t fusemap[FUSE_MAX / (8 * sizeof(uint32_t))];
// 	uint32_t infomap[INFO_MAX / (8 * sizeof(uint32_t))];
// 	uint32_t epoch_;      /* word 7 of FWR */
// 	uint32_t major_;      /* keyladder count */
// 	uint32_t minor_;
// 	uint64_t timestamp_;  /* time of signing */
// 	uint32_t p4cl_;
// 	/* bits to and with FUSE_FW_DEFINED_BROM_APPLYSEC */
// 	uint32_t applysec_;
// 	/* bits to mesh with FUSE_FW_DEFINED_BROM_CONFIG1 */
// 	uint32_t config1_;
// 	/* bits to or with FUSE_FW_DEFINED_BROM_ERR_RESPONSE */
// 	uint32_t err_response_;
// 	/* action to take when expectation is violated */
// 	uint32_t expect_response_;

// 	union {
// 		// 2nd FIPS signature (gnubby RW / Cr51)
// 		struct {
// 			uint32_t keyid;
// 			uint32_t r[8];
// 			uint32_t s[8];
// 		} ext_sig;

// 		// FLASH trim override (Dauntless RO)
// 		// iff config1_ & 65536
// 		struct {
// 			uint32_t FSH_SMW_SETTING_OPTION3;
// 			uint32_t FSH_SMW_SETTING_OPTION2;
// 			uint32_t FSH_SMW_SETTING_OPTIONA;
// 			uint32_t FSH_SMW_SETTING_OPTIONB;
// 			uint32_t FSH_SMW_SMP_WHV_OPTION1;
// 			uint32_t FSH_SMW_SMP_WHV_OPTION0;
// 			uint32_t FSH_SMW_SME_WHV_OPTION1;
// 			uint32_t FSH_SMW_SME_WHV_OPTION0;
// 		} fsh;
// 	} u;

// 	/* Padding to bring the total structure size to 1K. */
// 	uint32_t _pad[5];
// 	struct {
// 		unsigned size:12;
// 		unsigned offset:20;
// 	} swap_mark;

// 	/* Field for managing updates between RW product families. */
// 	uint32_t rw_product_family_;
// 	/* Board ID type, mask, flags (stored ^SIGNED_HEADER_PADDING) */
// 	uint32_t board_id_type;
// 	uint32_t board_id_type_mask;
// 	uint32_t board_id_flags;
// 	uint32_t dev_id0_;    /* node id, if locked */
// 	uint32_t dev_id1_;
// 	uint32_t fuses_chk_;  /* top 32 bit of expected fuses hash */
// 	uint32_t info_chk_;   /* top 32 bit of expected info hash */