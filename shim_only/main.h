#include <linux/types.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/cdefs.h>
#include <ctype.h>
#include <openssl/sha.h>
#ifndef MAIN_HEADER
#define MAIN_HEADER
struct sysinfo_s {
	uint32_t ro_keyid;
	uint32_t rw_keyid;
	uint32_t dev_id0;
	uint32_t dev_id1;
} __packed;

/*
 * This file contains structures used to facilitate cr50 firmware updates,
 * which can be used on any g chip.
 *
 * The firmware update protocol consists of two phases: connection
 * establishment and actual image transfer.
 *
 * Image transfer is done in 1K blocks. The host supplying the image
 * encapsulates blocks in frames by prepending a header including the flash
 * offset where the block is destined and its digest.
 *
 * The CR50 device responds to each frame with a confirmation which is 1 byte
 * response. Zero value means success, non zero value is the error code
 * reported by CR50.
 *
 * To establish the connection, the host sends a different frame, which
 * contains no data and is destined to offset 0. Receiving such a frame
 * signals the CR50 that the host intends to transfer a new image.
 *
 * The connection establishment response is described by the
 * first_response_pdu structure below.
 */

#define UPGRADE_PROTOCOL_VERSION 6
#define __packed __attribute__((packed))
/* This is the format of the update frame header. */
struct upgrade_command {
	uint32_t  block_digest;  /* first 4 bytes of sha1 of the rest of the
				  * frame.
				  */
	uint32_t  block_base;    /* Offset of this frame into the flash SPI. */
	/* The actual payload goes here. */
} __attribute__((packed));
#define FUSE_PADDING 0x55555555  /* baked in hw! */
#define FUSE_IGNORE 0xa3badaac   /* baked in rom! */
#define FUSE_MAX 128             /* baked in rom! */

#define INFO_MAX 128             /* baked in rom! */
#define INFO_IGNORE 0xaa3c55c3   /* baked in rom! */

#define MAGIC_HAVEN 0xFFFFFFFF
#define MAGIC_DAUNTLESS 0xFFFFFFFD

/* Default value for _pad[] words */
#define SIGNED_HEADER_PADDING 0x33333333

/*
 * This is the frame format the host uses when sending update PDUs over USB.
 *
 * The PDUs are up to 1K bytes in size, they are fragmented into USB chunks of
 * 64 bytes each and reassembled on the receive side before being passed to
 * the flash update function.
 *
 * The flash update function receives the unframed PDU body (starting at the
 * cmd field below), and puts its reply into the same buffer the PDU was in.
 */
struct update_frame_header {
	uint32_t block_size;    /* Total size of the block, including this
				 * field.
				 */
	struct upgrade_command cmd;
};

/*
 * A convenience structure which allows to group together various revision
 * fields of the header created by the signer.
 *
 * These fields are compared when deciding if versions of two images are the
 * same or when deciding which one of the available images to run.
 */
struct signed_header_version {
	uint32_t minor;
	uint32_t major;
	uint32_t epoch;
};

/*
 * Response to the connection establishment request.
 *
 * When responding to the very first packet of the upgrade sequence, the
 * original USB update implementation was responding with a four byte value,
 * just as to any other block of the transfer sequence.
 *
 * It became clear that there is a need to be able to enhance the upgrade
 * protocol, while staying backwards compatible.
 *
 * All newer protocol versions (starting with version 2) respond to the very
 * first packet with an 8 byte or larger response, where the first 4 bytes are
 * a version specific data, and the second 4 bytes - the protocol version
 * number.
 *
 * This way the host receiving of a four byte value in response to the first
 * packet is considered an indication of the target running the 'legacy'
 * protocol, version 1. Receiving of an 8 byte or longer response would
 * communicates the protocol version in the second 4 bytes.
 */
struct first_response_pdu {
	uint32_t return_value;

	/* The below fields are present in versions 2 and up. */
	uint32_t protocol_version;

	/* The below fields are present in versions 3 and up. */
	uint32_t  backup_ro_offset;
	uint32_t  backup_rw_offset;

	/* The below fields are present in versions 4 and up. */
	/* Versions of the currently active RO and RW sections. */
	struct signed_header_version shv[2];

	/* The below fields are present in versions 5 and up */
	/* keyids of the currently active RO and RW sections. */
	uint32_t keyid[2];
};

#define UPGRADE_DONE          0xB007AB1E

void fw_upgrade_command_handler(void *body,
				size_t cmd_size,
				size_t *response_size);

/* Used to tell fw upgrade the update ran successfully and is finished */
void fw_upgrade_complete(void);

/* Verify integrity of the PDU received over USB. */
int usb_pdu_valid(struct upgrade_command *cmd_body,
		  size_t cmd_size);

/* Various upgrade command return values. */
enum return_value {
	UPGRADE_SUCCESS = 0,
	UPGRADE_BAD_ADDR = 1,
	UPGRADE_ERASE_FAILURE = 2,
	UPGRADE_DATA_ERROR = 3,
	UPGRADE_WRITE_FAILURE = 4,
	UPGRADE_VERIFY_ERROR = 5,
	UPGRADE_GEN_ERROR = 6,
	UPGRADE_MALLOC_ERROR = 7,
	UPGRADE_ROLLBACK_ERROR = 8,
	UPGRADE_RATE_LIMIT_ERROR = 9,
	UPGRADE_UNALIGNED_BLOCK_ERROR = 10,
	UPGRADE_TRUNCATED_HEADER_ERROR = 11,
	UPGRADE_BOARD_ID_ERROR = 12,
	UPGRADE_BOARD_FLAGS_ERROR = 13,
	UPGRADE_DEV_ID_MISMATCH_ERROR = 14,
};
static const char* upgrade_error_str[]	 = {
"UPGRADE_SUCCESS",
"UPGRADE_BAD_ADDR",
"UPGRADE_ERASE_FAILURE",
"UPGRADE_DATA_ERROR",
"UPGRADE_WRITE_FAILURE",
"UPGRADE_VERIFY_ERROR",
"UPGRADE_GEN_ERROR",
"UPGRADE_MALLOC_ERROR",
"UPGRADE_ROLLBACK_ERROR",
"UPGRADE_RATE_LIMIT_ERROR",
"UPGRADE_UNALIGNED_BLOCK_ERROR",
"UPGRADE_TRUNCATED_HEADER_ERROR",
"UPGRADE_BOARD_ID_ERROR",
"UPGRADE_BOARD_FLAGS_ERROR",
"UPGRADE_DEV_ID_MISMATCH_ERROR"
};
/*
 * This is the size of the update frame payload, unless this is the last chunk
 * of the image.
 */
#define SIGNED_TRANSFER_SIZE 1024


/* Copyright 2014 The ChromiumOS Authors
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

#ifndef __CROS_EC_CONFIG_CHIP_H
#define __CROS_EC_CONFIG_CHIP_H

#if defined(BOARD)
#include "core/cortex-m/config_core.h"
#include "hw_regdefs.h"
#endif

/* Describe the RAM layout */
#define CONFIG_RAM_BASE         0x10000
#define CONFIG_RAM_SIZE         0x10000

/* Flash chip specifics */
#define CONFIG_FLASH_BANK_SIZE         0x800	/* protect bank size */
#define CONFIG_FLASH_ERASE_SIZE        0x800	/* erase bank size */
/* This flash can only be written as 4-byte words (aligned properly, too). */
#define CONFIG_FLASH_WRITE_SIZE        4	/* min write size (bytes) */
/* But we have a 32-word buffer for writing multiple adjacent cells */
#define CONFIG_FLASH_WRITE_IDEAL_SIZE  128	/* best write size (bytes) */
/* The flash controller prevents bulk writes that cross row boundaries */
#define CONFIG_FLASH_ROW_SIZE          256	/* row size */

/* Describe the flash layout */
#define CONFIG_PROGRAM_MEMORY_BASE     0x40000
#define CONFIG_FLASH_SIZE              (512 * 1024)
#define CONFIG_FLASH_ERASED_VALUE32    (-1U)
#define CONFIG_RO_HEAD_ROOM	       1024	/* Room for ROM signature. */
#define CONFIG_RW_HEAD_ROOM	       CONFIG_RO_HEAD_ROOM  /* same for RW */

/* Memory-mapped internal flash */
#define CONFIG_INTERNAL_STORAGE
#define CONFIG_MAPPED_STORAGE

/* Program is run directly from storage */
#define CONFIG_MAPPED_STORAGE_BASE CONFIG_PROGRAM_MEMORY_BASE

/* Interval between HOOK_TICK notifications */
#define HOOK_TICK_INTERVAL_MS 500
#define HOOK_TICK_INTERVAL    (HOOK_TICK_INTERVAL_MS * MSEC)

/* System stack size */
#define CONFIG_STACK_SIZE 1024

/* Idle task stack size */
#define IDLE_TASK_STACK_SIZE 512

/* Default task stack size */
#define TASK_STACK_SIZE 488

/* Larger task stack size, for hook task */
#define LARGER_TASK_STACK_SIZE 640

/* Magic for gpio.inc */
#define GPIO_PIN(port, index) (port), (1 << (index))
#define GPIO_PIN_MASK(port, mask) (port), (mask)
#define PLACEHOLDER_GPIO_BANK 0

#define PCLK_FREQ  (24 * 1000 * 1000)

/* Number of IRQ vectors on the NVIC */
#define CONFIG_IRQ_COUNT (GC_INTERRUPTS_COUNT - 15)

/* We'll have some special commands of our own */
#define CONFIG_EXTENSION_COMMAND 0xbaccd00a

/* Chip needs to do custom pre-init */
#define CONFIG_CHIP_PRE_INIT

/*
 * The flash memory is implemented in two halves. The SoC bootrom will look for
 * the first-stage bootloader at the beginning of each of the two halves and
 * prefer the newer one if both are valid. In EC terminology the bootloader
 * would be called the RO firmware, so we actually have two, not one. The
 * bootloader also looks in each half of the flash for a valid RW firmware, so
 * we have two possible RW images as well. The RO and RW images are not tightly
 * coupled, so either RO image can choose to boot either RW image.
 *
 * The EC firmware configuration is not (yet?) prepared to handle multiple,
 * non-contiguous, RO/RW combinations, so there's a bit of hackery to make this
 * work.
 *
 * The following macros try to make this all work.
 */

/* This isn't optional, since the bootrom will always look for both */
#define CHIP_HAS_RO_B

/* It's easier for us to consider each half as having its own RO and RW */
#define CFG_FLASH_HALF (CONFIG_FLASH_SIZE >> 1)

/*
 * We'll reserve some space at the top of each flash half for persistent
 * storage and other stuff that's not part of the RW image. We don't promise to
 * use these two areas for the same thing, it's just more convenient to make
 * them the same size.
 */
#define CFG_TOP_SIZE  0x3000
#define CFG_TOP_A_OFF (CFG_FLASH_HALF - CFG_TOP_SIZE)
#define CFG_TOP_B_OFF (CONFIG_FLASH_SIZE - CFG_TOP_SIZE)

/* The RO images start at the very beginning of each flash half */
#define CONFIG_RO_MEM_OFF 0
#define CHIP_RO_B_MEM_OFF CFG_FLASH_HALF

/* Size reserved for each RO image */
#define CONFIG_RO_SIZE 0x4000

/*
 * RW images start right after the reserved-for-RO areas in each half, but only
 * because that's where the RO images look for them. It's not a HW constraint.
 */
#define CONFIG_RW_MEM_OFF CONFIG_RO_SIZE
#define CONFIG_RW_B_MEM_OFF (CFG_FLASH_HALF + CONFIG_RW_MEM_OFF)

/* Size reserved for each RW image */
#define CONFIG_RW_SIZE (CFG_FLASH_HALF - CONFIG_RW_MEM_OFF - CFG_TOP_SIZE)

/*
 * These are needed in a couple of places, but aren't very meaningful. Because
 * we have two RO and two RW images, these values don't really match what's
 * described in the EC Image Geometry Spec at www.chromium.org.
 */
/* TODO(wfrichar): Make them meaningful or learn to do without */
#define CONFIG_EC_PROTECTED_STORAGE_OFF    0
#define CONFIG_EC_PROTECTED_STORAGE_SIZE   CONFIG_FLASH_SIZE
#define CONFIG_EC_WRITABLE_STORAGE_OFF     0
#define CONFIG_EC_WRITABLE_STORAGE_SIZE	   CONFIG_FLASH_SIZE
#define CONFIG_RO_STORAGE_OFF              0
#define CONFIG_RW_STORAGE_OFF              0
#define CONFIG_WP_STORAGE_OFF		   0
#define CONFIG_WP_STORAGE_SIZE		   CONFIG_EC_PROTECTED_STORAGE_SIZE

/*
 * Note: early versions of the SoC would let us build and manually sign our own
 * bootloaders, and the RW images could be self-signed. Production SoCs require
 * officially-signed binary blobs to use for the RO bootloader(s), and the RW
 * images that we build must be manually signed. So even though we generate RO
 * firmware images, they may not be useful.
 */
#define CONFIG_CUSTOMIZED_RO

/* Number of I2C ports */
#define I2C_PORT_COUNT 2

#define CONFIG_FLASH_LOG_SPACE CONFIG_FLASH_BANK_SIZE

/*
 * Flash log occupies space in the top of RO_B section, its counterpart in
 * RO_A is occupied by the certs.
 */
#define CONFIG_FLASH_LOG_BASE                                                  \
	(CONFIG_PROGRAM_MEMORY_BASE + CHIP_RO_B_MEM_OFF + CONFIG_RO_SIZE -     \
	 CONFIG_FLASH_LOG_SPACE)

/* Space reserved for RO hashes */
#define AP_RO_DATA_SPACE_SIZE CONFIG_FLASH_BANK_SIZE
#define AP_RO_DATA_SPACE_ADDR (CONFIG_FLASH_LOG_BASE - AP_RO_DATA_SPACE_SIZE)

/* Maximum space available for the RO image */
#define MAX_RO_CODE_SIZE (CONFIG_RO_SIZE - CONFIG_FLASH_LOG_SPACE - \
			  AP_RO_DATA_SPACE_SIZE)

/* Use software crypto (libcryptoc). */
#define CONFIG_LIBCRYPTOC
#endif /* __CROS_EC_CONFIG_CHIP_H */
#endif