cmake_minimum_required(VERSION 3.0.0)
project(crswifty VERSION 0.1.0)

include(CTest)
file(GLOB_RECURSE echeaders RELATIVE "ec/" "*.h")
add_library(libcurl1.a STATIC IMPORTED)
SET(USE_LIBCURL_FROM "" CACHE PATH "statically link libcurl")
SET(USE_LIBCURL_INCLUDE "" CACHE PATH "libcurl include dir")
set_property(TARGET libcurl1.a PROPERTY IMPORTED_LOCATION ${USE_LIBCURL_FROM})
set_property(TARGET libcurl1.a PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${USE_LIBCURL_INCLUDE})
include_directories(include/** /usr/include/dbus-1.0/ /usr/include/dbus-c++-1/ /usr/include/x86_64-linux-gnu/ ${echeaders})
link_libraries(libcurl1.a libssl.a libcrypto.a dl pthread)
enable_testing()

add_compile_options(
)
# It used to be rma_host but now it is shim_only main.cpp until we find a valid use after free within the browser process.
add_executable(crswiftyterminal shim_only/term.cpp)
add_executable(crswifty shim_only/main.cc)

set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
include(CPack)

